#################################
# create package
#################################


# generate a package skeleton:
#package.skeleton("stoichcalc",code_files="stoichcalc.r")

# check package:
system("R CMD check stoichcalc")

# build source package (.tar.gz):
system("R CMD build stoichcalc")

# check processed package:
system("R CMD check --as-cran stoichcalc_1.1-5.tar.gz")

# build binary package for Windows (.zip):
# system("R CMD INSTALL --build stoichcalc")

install.packages("stoichcalc_1.1-5.tar.gz",repos=NULL,type="source")
library(stoichcalc)


# upload:
# https://cran.r-project.org/submit.html


